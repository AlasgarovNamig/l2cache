package az.ingressunibank.L2Cache.controller;

import az.ingressunibank.L2Cache.model.Student;
import az.ingressunibank.L2Cache.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Cacheable;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
@Slf4j
@EnableCaching
public class StudentController {

    private final StudentRepository studentRepository;

    @GetMapping("/al")

    public Student getUser() {
        return studentRepository.findById(1l).get();
    }
    @GetMapping("/ma")
    public Student getUser2() {
        return studentRepository.findByName("alma").get();
    }
}
