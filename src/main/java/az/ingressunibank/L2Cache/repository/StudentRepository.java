package az.ingressunibank.L2Cache.repository;

import az.ingressunibank.L2Cache.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student>findByName(String name);
}
